package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
	
	/*nao funciona para frases, porem nao quis colar da internet uma solucao que fizesse
	 * funcionar com frases*/
	
	public static void main(String[] args) {
		String madam = "madam";
		String ovo = "ovo";
		String racecar = "racecar";
		String nao = "nao";
		int numero = 121;
		int numero2 = 321;
		
		ehPalindromo(madam);
		ehPalindromo(racecar);
		ehPalindromo(ovo);
		ehPalindromo(nao);
		ehPalindromo(String.valueOf(numero));
		ehPalindromo(String.valueOf(numero2));
		
	}

	private static void ehPalindromo(String elemento) {
		StringBuffer buffer = new StringBuffer();

		for (int i = (elemento.toCharArray().length-1); i >= 0; i--) {
			buffer.append(elemento.toCharArray()[i]);
			System.out.println(elemento.toCharArray()[i]);
		}
		
		if (elemento.equalsIgnoreCase(String.valueOf(buffer))) {
			System.out.println("É palíndromo: " + elemento);
		}else {
			System.out.println("Não é palíndromo: " + elemento);
		}
	}
}
