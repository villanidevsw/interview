package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {

	/*
	 * acho que nao tem como remover o elemento do meio da lista, pois se a lista
	 * tiver tamanho por exemplo: 5, quem seria o elemento do meio? ou se tiver
	 * tamanho 7?
	 * 
	 */

	public static void main(String[] args) {
		List<String> lista = new ArrayList<String>(Arrays.asList("uva", "maçã", "pera", "abacaxi"));

		for (String elementoAprocurar : lista) {
			exibirAnteriorEProximo(lista, elementoAprocurar);
		}

	}

	private static void exibirAnteriorEProximo(List<String> lista, String elementoAprocurar) {
		if (!lista.isEmpty()) {
			int primeiroIndiceDaLista = 0;
			int ultimoIndiceDaLista = lista.size() - 1;

			if (lista.contains(elementoAprocurar)) {
				int indiceDoElemento = lista.indexOf(elementoAprocurar);

				exibeAnterior(lista, elementoAprocurar, primeiroIndiceDaLista, indiceDoElemento);

				exibeProximo(lista, elementoAprocurar, ultimoIndiceDaLista, indiceDoElemento);

				System.out.println(lista);
			}
		}
	}

	private static void exibeProximo(List<String> lista, String elementoAprocurar, int ultimoIndiceDaLista,
			int indiceDoElemento) {
		if (!(indiceDoElemento + 1 > ultimoIndiceDaLista)) {
			System.out.println(
					"item seguinte do elemento: " + elementoAprocurar + " é " + lista.get(indiceDoElemento + 1));
		} else {
			System.out.println("item: " + elementoAprocurar + " é o ultimo item");
		}
	}

	private static void exibeAnterior(List<String> lista, String elementoAprocurar, int primeiroIndiceDaLista,
			int indiceDoElemento) {
		if (!(indiceDoElemento - 1 < primeiroIndiceDaLista)) {
			System.out.println(
					"item anterior do elemento: " + elementoAprocurar + " é " + lista.get(indiceDoElemento - 1));
		} else {
			System.out.println("item: " + elementoAprocurar + " é o primeiro item");
		}
	}

}
