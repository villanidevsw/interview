package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

	public static void main(String[] args) {
		List<String> lista = new ArrayList<String>(Arrays.asList("clio", "gol", "celta", "celta","palio","palio"));
		
		Set<String> distintos = new HashSet<String>(lista);
		System.out.println(distintos);

	}
}
