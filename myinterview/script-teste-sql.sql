use employees;
show tables;
-- 1)Query que retorna a quantidade de funcionários separados por sexo.
SELECT gender , count(*) FROM employees GROUP by gender;

-- 2)Query que retorna a quantidade de funcionários distintos por sexo, ano e ano de nascimento. 
select distinct first_name,last_name,gender,hire_date,birth_date from employees;

-- 3) Query que retorna a média, min e max de salário por sexo.
select sub1.gender,avg(sub1.salary), min(sub1.salary), max(sub1.salary) from 
(SELECT e.gender, s.salary 
FROM employees e inner join salaries s on s.emp_no = e.emp_no where e.gender = 'M') sub1
union 
select sub2.gender, avg(sub2.salary), min(sub2.salary), max(sub2.salary) from
(SELECT e.gender, s.salary
FROM employees e inner join salaries s on s.emp_no = e.emp_no where e.gender = 'F')sub2;









